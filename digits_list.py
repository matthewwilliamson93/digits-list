from math import floor, log10
from typing import Dict, Iterator, List


def comp(num: int) -> List[int]:
    return [int(i) for i in str(num)]


def comp_repr(num: int) -> List[int]:
    return [int(i) for i in num.__repr__()]


_ZERO = ord('0')


def comp_ord(num: int) -> List[int]:
    z = _ZERO
    return [ord(i) - z for i in num.__repr__()]


_DICT_MAP: Dict[str, int] = {
    '0': 0,
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9
}


def comp_dict_map(num: int) -> List[int]:
    dm = _DICT_MAP
    return [dm[i] for i in num.__repr__()]


def gen_dict_map(num: int) -> Iterator[int]:
    dm = _DICT_MAP
    return (dm[i] for i in num.__repr__())


def div_and_mod(num: int) -> List[int]:
    lst = []
    while num:
        lst.append(num % 10)
        num //= 10
    lst.reverse()
    return lst


def gen_div_and_mod(num: int) -> Iterator[int]:
    lst = []
    while num:
        lst.append(num % 10)
        num //= 10
    return reversed(lst)


def ldivmod(num: int) -> List[int]:
    lst = []
    while num:
        num, d = divmod(num, 10)
        lst.append(d)
    lst.reverse()
    return lst


def div_and_mod_preallocate_log(num: int) -> List[int]:
    size = floor(log10(num)) + 1
    lst = [0] * size
    for i in range(size - 1, -1, -1):
        lst[i] = num % 10
        num //= 10
    return lst


def div_and_mod_preallocate_log_while(num: int) -> List[int]:
    i = floor(log10(num))
    lst = [0] * (i + 1)
    while i != -1:
        lst[i] = num % 10
        num //= 10
        i -= 1
    return lst


def div_and_mod_preallocate_ifs_i(num: int) -> List[int]:
    if num < 10:
        size = 1
    elif num < 100:
        size = 2
    elif num < 1000:
        size = 3
    elif num < 10000:
        size = 4
    elif num < 100000:
        size = 5
    elif num < 1000000:
        size = 6
    elif num < 10000000:
        size = 7
    elif num < 100000000:
        size = 8
    elif num < 1000000000:
        size = 9
    else:
        size = 10
    lst = [0] * size
    for i in range(size - 1, -1, -1):
        lst[i] = num % 10
        num //= 10
    return lst


def div_and_mod_preallocate_ifs_d(num: int) -> List[int]:
    if num > 999999999:
        size = 10
    elif num > 99999999:
        size = 9
    elif num > 9999999:
        size = 8
    elif num > 999999:
        size = 7
    elif num > 99999:
        size = 6
    elif num > 9999:
        size = 5
    elif num > 999:
        size = 4
    elif num > 99:
        size = 3
    elif num > 9:
        size = 2
    else:
        size = 1
    lst = [0] * size
    for i in range(size - 1, -1, -1):
        lst[i] = num % 10
        num //= 10
    return lst


def div_and_mod_preallocate_bchops(num: int) -> List[int]:
    size = 1
    n = num
    if n >= 100000000:
        size += 8
        n //= 100000000
    if n >= 10000:
        size += 4
        n //= 10000
    if n >= 100:
        size += 2
        n //= 100
    if n >= 10:
        size += 1

    lst = [0] * size
    for i in range(size - 1, -1, -1):
        lst[i] = num % 10
        num //= 10
    return lst


def div_and_mod_preallocate_bsearch(num: int) -> List[int]:
    if num < 100000:
        if num < 1000:
            if num < 10:
                size = 1
            elif num < 100:
                size = 2
            else:
                size = 3
        else:
            if num < 10000:
                size = 4
            else:
                size = 5
    else:
        if num < 10000000:
            if num < 1000000:
                size = 6
            else:
                size = 7
        else:
            if num < 100000000:
                size = 8
            elif num < 1000000000:
                size = 9
            else:
                size = 10

    lst = [0] * size
    for i in range(size - 1, -1, -1):
        lst[i] = num % 10
        num //= 10
    return lst


def div_and_mod_preallocate_bsearch_modified(num: int) -> List[int]:
    if num > 99:
        if num > 999999:
            if num > 99999999:
                size = 9 + (num > 999999999)
            else:
                size = 7 + (num > 9999999)
        else:
            if num > 9999:
                size = 5 + (num > 99999)
            else:
                size = 3 + (num > 999)
    else:
        size = 1 + (num > 9)

    lst = [0] * size
    for i in range(size - 1, -1, -1):
        lst[i] = num % 10
        num //= 10
    return lst
