# Digits List

This is a benchmark suite to determine what is the most efficient way to
iterate the digits of an integer.

I apologize, but I have already put too much time into this to document
why I made the implementations the way that I did or to do a good write up,
but I did post the results.

## Results

```
+------------------------------------------+---------+---------+---------+
| Functions                                |   Small |  Medium |   Large |
+------------------------------------------+---------+---------+---------+
| comp                                     | 1.00556 | 1.60107 | 2.19007 |
| comp_repr                                | 0.99281 | 1.54083 | 2.06211 |
| comp_ord                                 | 0.68712 |  0.9214 |  1.2004 |
| comp_dict_map                            | 0.59624 | 0.73376 | 0.88531 |
| gen_dict_map                             | 0.69469 | 0.88457 | 1.07334 |
| div_and_mod                              | 0.50261 | 0.87198 | 1.22994 |
| gen_div_and_mod                          |   0.585 | 0.95042 | 1.31105 |
| ldivmod                                  |  0.7622 | 1.39536 | 2.02142 |
| div_and_mod_preallocate_log              | 0.82504 | 1.03901 |  1.2698 |
| div_and_mod_preallocate_log_while        | 0.68447 | 0.98844 | 1.33536 |
| div_and_mod_preallocate_ifs_i            | 0.74159 | 1.02363 | 1.31708 |
| div_and_mod_preallocate_ifs_d            | 0.83742 | 1.02322 | 1.16908 |
| div_and_mod_preallocate_bchops           | 0.77952 | 1.02316 | 1.26195 |
| div_and_mod_preallocate_bsearch          | 0.74525 | 0.94103 | 1.19002 |
| div_and_mod_preallocate_bsearch_modified | 0.74861 | 0.97814 | 1.21173 |
+------------------------------------------+---------+---------+---------+
```

## Test

There is a Makefile present which you should just be able to run `make test`
to test this yourself. This is run with `python3.6`, and uses `vulture`, `mypy`, `pytest`,
and `prettytable`.
