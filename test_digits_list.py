#!/usr/bin/env python3.6

import random
import time
from typing import Callable, Iterable, List

from prettytable import PrettyTable

from digits_list import *


def test_comp():
    assert [1, 2, 3, 4, 5] == comp(12345)


def test_comp_repr():
    assert [1, 2, 3, 4, 5] == comp_repr(12345)


def test_comp_ord():
    assert [1, 2, 3, 4, 5] == comp_ord(12345)


def test_comp_dict_map():
    assert [1, 2, 3, 4, 5] == comp_dict_map(12345)


def test_gen_dict_map():
    assert [1, 2, 3, 4, 5] == list(gen_dict_map(12345))


def test_div_and_mod():
    assert [1, 2, 3, 4, 5] == div_and_mod(12345)


def test_gen_div_and_mod():
    assert [1, 2, 3, 4, 5] == list(gen_div_and_mod(12345))


def test_ldivmod():
    assert [1, 2, 3, 4, 5] == ldivmod(12345)


def test_div_and_mod_preallocate_log():
    assert [1, 2, 3, 4, 5] == div_and_mod_preallocate_log(12345)


def test_div_and_mod_preallocate_log_while():
    assert [1, 2, 3, 4, 5] == div_and_mod_preallocate_log_while(12345)


def test_div_and_mod_preallocate_ifs_i():
    assert [1, 2, 3, 4, 5] == div_and_mod_preallocate_ifs_i(12345)


def test_div_and_mod_preallocate_ifs_d():
    assert [1, 2, 3, 4, 5] == div_and_mod_preallocate_ifs_d(12345)


def test_div_and_mod_preallocate_bchops():
    assert [1, 2, 3, 4, 5] == div_and_mod_preallocate_bchops(12345)


def test_div_and_mod_preallocate_bsearch():
    assert [1, 2, 3, 4, 5] == div_and_mod_preallocate_bsearch(12345)


def test_div_and_mod_preallocate_bsearch_modified():
    assert [1, 2, 3, 4, 5] == div_and_mod_preallocate_bsearch_modified(12345)


def bench(fn: Callable[[int], Iterable[int]], numbers: List[int]) -> float:
    start = time.time()
    for i in numbers:
        for _ in fn(i):
            pass
    total = round(time.time() - start, 5)
    print('.', end='', flush=True)
    return total


def main():
    numbers = [[random.randint(1, 999) for _ in range(1000000)],
               [random.randint(1000, 999999) for _ in range(1000000)],
               [random.randint(1000000, 999999999) for _ in range(1000000)]]

    functions = [comp,
                 comp_repr,
                 comp_ord,
                 comp_dict_map,
                 gen_dict_map,
                 div_and_mod,
                 gen_div_and_mod,
                 ldivmod,
                 div_and_mod_preallocate_log,
                 div_and_mod_preallocate_log_while,
                 div_and_mod_preallocate_ifs_i,
                 div_and_mod_preallocate_ifs_d,
                 div_and_mod_preallocate_bchops,
                 div_and_mod_preallocate_bsearch,
                 div_and_mod_preallocate_bsearch_modified]

    print('Benchmarking ', end='', flush=True)
    table = PrettyTable()
    table.field_names = ['Functions', 'Small', 'Medium', 'Large']
    table.align['Functions'] = 'l'
    table.align['Small'] = 'r'
    table.align['Medium'] = 'r'
    table.align['Large'] = 'r'

    for fn in functions:
        table.add_row([fn.__name__] + [bench(fn, i) for i in numbers])

    print()
    print(table)


if __name__ == '__main__':
    main()
