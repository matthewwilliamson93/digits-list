.PHONY: test
test:
	@python3.6 -m vulture --min-confidence=90 .
	@python3.6 -m mypy --ignore-missing-imports .
	@python3.6 -m pytest
	@./test_digits_list.py

.PHONY: clean
clean:
	@rm -rf *.pyc __pycache__ .mypy_cache .pytest_cache
